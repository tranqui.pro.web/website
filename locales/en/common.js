export default {
  'site-title': 'Kuarahy Club',
  'site-subtitle': 'Subtitle',
  'leer-mas': 'Leer más',
  'conoce-mas': 'Conocé más',
  'terminos-y-politica-de-privacidad': 'Términos de uso y política de privacidad',
  'preguntas-frecuentes': 'Preguntas Frecuentes',
  'escribinos': 'Escribinos',
  'hecho-con-amor': 'Hecho con ',
  'en-paraguay': ' en Paraguay',
  'menu' : {
    'inicio': 'Inicio'
  },
  'locale-select': {
    'es': 'Español',
    'en': 'Inglés'
  },
  'pages': {
    'politica-de-privacidad': 'Política de privacidad',
    'terminos-y-condiciones': 'Términos y Condiciones'
  }
}
