import Layout from '../components/layout'
import Link from 'next/link'
import SectionPostHeader from "../components/sectionPostHeader"
import SectionFooter from "../components/sectionFooter"
import withLocale from '../lib/translations/withLocale.hoc'
import { readLocaleStrings } from '../lib/translations/readLocaleStrings'
import useTranslation from '../lib/translations/useTranslation.hook'
import config from '../config'

export function Custom404() {
  const { t, locale } = useTranslation()
  return (
    <Layout title='404'>
      <SectionPostHeader 
        imageUrl={`/assets/hero-portada.png`}
      />
      
      <section>
        <div className="text-center items-top py-36 pt-24">
          <h2 className=" text-2xl">{t('errors.404')}</h2>
          <Link href={`/${locale}`}>
            <a className="underline inline-block mt-10 px-4 py-1.5 text-sm font-semibold text-brand-pink-3 bg-white border-2 border-brand-pink-3 rounded-lg">
              Ir al inicio
            </a>
          </Link>
        </div>
      </section>

      <SectionFooter id="footer"></SectionFooter>
    </Layout>
  )
}

export async function getStaticProps(props) {
  return {
    props: {
      locale: config.defaultLocale,
      localeStrings: readLocaleStrings(config.defaultLocale, ['common', 'errors'])
    }
  }
}

export default withLocale(Custom404)