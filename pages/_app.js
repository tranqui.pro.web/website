import '../styles/globals.css'
import withLayout from '../components/withLayout.hoc'

export function App({ Component, pageProps }) {
  return <>
    <Component {...pageProps} />
  </>
}

export default withLayout(App)