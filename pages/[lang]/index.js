import React from 'react'
import _ from 'lodash'
import ReactMarkdown from 'react-markdown'
import Layout from '../../components/layout'
import withLocale from '../../lib/translations/withLocale.hoc'
import config from '../../config'
import { readLocaleStrings } from '../../lib/translations/readLocaleStrings'
import useTranslation, { getPostContentByLocaledURL } from '../../lib/translations/useTranslation.hook'
import HeaderMain from '../../components/headerMain'
import SectionFooter from '../../components/sectionFooter'
import Beneficios from '../../components/beneficios'
import Proyectos from '../../components/proyectos'
import Tecnologias from '../../components/tecnologias'
import Componentes from '../../components/componentes'
import Presupuesto from '../../components/presupuesto'


export function BlogPost(props) {
  const { t, locale } = useTranslation()
  const { portadaContent } = props
  const data = portadaContent;
  
  return (
    <Layout home>
      <HeaderMain data={data}></HeaderMain>

      <Beneficios data={data}></Beneficios>
      <Proyectos data={data}></Proyectos>
      <Tecnologias data={data}></Tecnologias>
      {/* <Componentes data={data}></Componentes> */}

      <Presupuesto data={data}></Presupuesto>
    </Layout>
  )
}

export async function getStaticPaths () {
  const paths = []
  config.locales.forEach(locale => {
    paths.push({ params: { lang: locale } })
  })
  return {
    paths, fallback: false
  }
}

export async function getStaticProps ( props ) {
  let { lang } = props.params
  return {
    props: {
      locale: lang,
      localeStrings: readLocaleStrings(lang, ['common', 'inicio']),
      portadaContent: getPostContentByLocaledURL('paginas', 'portada', lang)
    }
  }
}

export default withLocale(BlogPost)