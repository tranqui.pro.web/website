export default function Tecnologias({ data }) {
  
  return (
    <div id="tecnologias" className=" flex flex-col bg-white">
      {/* tecnologias */}
      <div className="py-6 mt-24 content-padding">
        <h2 className='ml-5 titles-padding'>
          <span className='title'>{data.text.tecnologias_1}</span>
          <br />
          <span className='sub-title'>{data.text.tecnologias_2}</span>
        </h2>
      </div>

      <div className="mt-20 grid grid-flow-row grid-cols-1 gap-2 content-padding">
        {/* caracteristica 1 */}
        <div className="mb-12 lg:mb-20">
          <h3 className='ml-5 titles-padding'>
            <span className='title'>{data.text.caracteristica_a_1}</span>
            <br />
            <span className='sub-title'>{data.text.caracteristica_a_2}</span>
          </h3>

          <div className=" mt-5 text-container leading-snug">
            {data.text.caracteristica_a_content}
          </div>
        </div>

        {/* caracteristica 2 */}
        <div className="">
          <h3 className='ml-5 titles-padding'>
            <span className='title'>{data.text.caracteristica_b_1}</span>
            <br />
            <span className='sub-title'>{data.text.caracteristica_b_2}</span>
          </h3>

          <div className=" mt-5 text-container leading-snug">
            {data.text.caracteristica_b_content}
          </div>
        </div>
      </div>

      <div className="content-padding">
        <div className="grid grid-flow-row grid-cols-2 lg:grid-cols-3 gap-2 sm:gap-6 px-10 lg:py-16 xl:py-24 text-center">
          {data.text.tecnologias_list.map(({ title, image, desc }, index) => (
            <div className=" font-brand-condensed mt-10" key={index}>
              <div className="">
                <img src={`/assets/${image}`} alt={title} className=" w-full md:w-56 lg:w-64 inline-block" />
              </div>
              <div className='lg:text-lg'>
                {desc}
              </div>
            </div>
          ))}
        </div>
      </div>

      {/* mockups images */}
      {/* ...movido a presupuesto */}
    </div>
  )
}