import AnimatedTrackByScroll from './animatedTrackByScroll'

export default function Beneficios({ data }) {
  
  return (
    <div id="beneficios" className=" flex flex-col bg-white">
      {/* abarca_mas */}
      <div className="py-6 mt-24 content-padding">
        <h2 className='ml-5 titles-padding'>
          <span className='title'>{data.text.abarca_mas_1}</span>
          <br />
          <span className='sub-title'>{data.text.abarca_mas_2}</span>
        </h2>

        <div className=" mt-5 text-container">
          {data.text.abarca_mas_content}
        </div>

        <a href={`https://wa.me/595991200644?text=${data.text.ctaLinkText[1]}`} target="_blank" rel="noreferrer" >
          <button className='cta mt-5 ml-5 bg-brand-celeste-2 text-white hover:bg-lightBlue-500'>{data.text.saber_mas}</button>
        </a>
      </div>

      {/* mockups images */}
      <div className="mt-24 bg-gradient-to-br from-brand-turquesa to-brand-amarillo bg-fixed">
        {/* track */}
        <AnimatedTrackByScroll className="py-10 p-5 lg:pl-32 lg:py-12 xl:pl-48 2xl:pl-72 2xl:py-16" getTopOffset={() => (50)}>
          <img src="/assets/mockups/mockup-16.jpg" className=' w-[200px] md:w-[230px] lg:w-[270px] lg:mr-10 xl:w-[300px] xl:mr-16 2xl:w-[340px] 2xl:mr-20 rounded-lg mr-5' />
          <img src="/assets/mockups/mockup-17.jpg" className=' w-[200px] md:w-[230px] lg:w-[270px] lg:mr-10 xl:w-[300px] xl:mr-16 2xl:w-[340px] 2xl:mr-20 rounded-xl mr-5' />
          <img src="/assets/mockups/mockup-15.jpg" className=' w-[200px] md:w-[230px] lg:w-[270px] lg:mr-10 xl:w-[300px] xl:mr-16 2xl:w-[340px] 2xl:mr-20 rounded-lg mr-5' />
          <img src="/assets/mockups/mockup-18.jpg" className=' w-[200px] md:w-[230px] lg:w-[270px] lg:mr-10 xl:w-[300px] xl:mr-16 2xl:w-[340px] 2xl:mr-20 rounded-lg mr-5' />
        </AnimatedTrackByScroll>
      </div>

      <div className="mt-20 grid grid-flow-row grid-cols-1 gap-2 content-padding">
        {/* beneficio 1 */}
        <div className="mb-12 lg:mb-20">
          <h3 className='ml-5 titles-padding'>
            <span className='title'>{data.text.beneficio_a_1}</span>
            <br />
            <span className='sub-title'>{data.text.beneficio_a_2}</span>
          </h3>

          <div className=" mt-5 text-container leading-snug">
            {data.text.beneficio_a_content}
          </div>
        </div>

        {/* beneficio 2 */}
        <div className="">
          <h3 className='ml-5 titles-padding'>
            <span className='title'>{data.text.beneficio_b_1}</span>
            <br />
            <span className='sub-title'>{data.text.beneficio_b_2}</span>
          </h3>

          <div className=" mt-5 text-container leading-snug">
            {data.text.beneficio_b_content}
          </div>
        </div>
      </div>
    </div>
  )
}