import useTranslation from '../lib/translations/useTranslation.hook'
import SectionFooterEnd from './sectionFooterEnd'

export default function SectionFooter({ page }) {
  const { t } = useTranslation()

  return (
    <footer id="footer" className=" bg-white/60 py-8 flex items-center md:place-content-center">
      <div className="text-left px-12 w-auto content-padding lg:pr-40 xl:pr-48 2xl:pr-56 lg:text-xl xl:text-2xl 2xl:text-3xl">
        <a href="https://instagram.com/tranqui.pro" target="_blank" rel="noreferrer" className='pb-2 block'>
          <img src="/assets/footer-logos/instagram.svg" className=' inline-block mr-2 w-6' />
          <span className=' inline-block mt-1 text-md hover:text-brand-azul hover:underline'>
            tranqui.pro
          </span>
        </a>
        <a href="https://facebook.com/tranqui.pro" target="_blank" rel="noreferrer" className='pb-2 block'>
          <img src="/assets/footer-logos/facebook.svg" className=' inline-block mr-2 w-6' />
          <span className=' inline-block mt-1 text-md hover:text-brand-azul hover:underline'>
            tranqui.pro
          </span>
        </a>
        <a href="mailto://tranqui.pro@gmail.com" target="_blank" rel="noreferrer" className='pb-2 block'>
          <img src="/assets/footer-logos/email.svg" className=' inline-block mr-2 w-6' />
          <span className=' inline-block mt-1 text-md hover:text-brand-azul hover:underline'>
            tranqui.pro@gmail.com
          </span>
        </a>
        <a href="https://wa.me/595991200644?text=Hola%20tranqui!" target="_blank" rel="noreferrer" className='pb-2 block'>
          <img src="/assets/footer-logos/whatsapp.svg" className=' inline-block mr-2 w-6' />
          <span className=' inline-block mt-1 text-md hover:text-brand-azul hover:underline'>
            +595 (991) 200 644
          </span>
        </a>
        <a href="https://www.tranqui.pro" target="_blank" rel="noreferrer" className='pb-2 block'>
          <img src="/assets/footer-logos/web.svg" className=' inline-block mr-2 w-6' />
          <span className=' inline-block mt-1 text-md hover:text-brand-azul hover:underline'>
            www.tranqui.pro
          </span>
        </a>
      </div>
      
      <div className={`
        hidden invisible
        
        p-0 p-px p-0.5 p-1 p-1.5 p-2 p-2.5 p-3 p-3.5 p-4 p-5 p-6 p-7 p-8 p-9 p-10 p-11 p-12 p-14 p-16 p-20 p-24 p-28 p-32 p-36 p-40
        pr-0 pr-px pr-0.5 pr-1 pr-1.5 pr-2 pr-2.5 pr-3 pr-3.5 pr-4 pr-5 pr-6 pr-7 pr-8 pr-9 pr-10 pr-11 pr-12 pr-14 pr-16 pr-20 pr-24 pr-28 pr-32 pr-36 pr-40
        pl-0 pl-px pl-0.5 pl-1 pl-1.5 pl-2 pl-2.5 pl-3 pl-3.5 pl-4 pl-5 pl-6 pl-7 pl-8 pl-9 pl-10 pl-11 pl-12 pl-14 pl-16 pl-20 pl-24 pl-28 pl-32 pl-36 pl-40

        -p-0 -p-px -p-0.5 -p-1 -p-1.5 -p-2 -p-2.5 -p-3 -p-3.5 -p-4 -p-5 -p-6 -p-7 -p-8 -p-9 -p-10 -p-11 -p-12 -p-14 -p-16 -p-20 -p-24 -p-28 -p-32 -p-36 -p-40
        -pr-0 -pr-px -pr-0.5 -pr-1 -pr-1.5 -pr-2 -pr-2.5 -pr-3 -pr-3.5 -pr-4 -pr-5 -pr-6 -pr-7 -pr-8 -pr-9 -pr-10 -pr-11 -pr-12 -pr-14 -pr-16 -pr-20 -pr-24 -pr-28 -pr-32 -pr-36 -pr-40
        -pl-0 -pl-px -pl-0.5 -pl-1 -pl-1.5 -pl-2 -pl-2.5 -pl-3 -pl-3.5 -pl-4 -pl-5 -pl-6 -pl-7 -pl-8 -pl-9 -pl-10 -pl-11 -pl-12 -pl-14 -pl-16 -pl-20 -pl-24 -pl-28 -pl-32 -pl-36 -pl-40

        m-0 m-mx m-0.5 m-1 m-1.5 m-2 m-2.5 m-3 m-3.5 m-4 m-5 m-6 m-7 m-8 m-9 m-10 m-11 m-12 m-14 m-16 m-20 m-24 m-28 m-32 m-36 m-40
        mr-0 mr-mx mr-0.5 mr-1 mr-1.5 mr-2 mr-2.5 mr-3 mr-3.5 mr-4 mr-5 mr-6 mr-7 mr-8 mr-9 mr-10 mr-11 mr-12 mr-14 mr-16 mr-20 mr-24 mr-28 mr-32 mr-36 mr-40
        ml-0 ml-mx ml-0.5 ml-1 ml-1.5 ml-2 ml-2.5 ml-3 ml-3.5 ml-4 ml-5 ml-6 ml-7 ml-8 ml-9 ml-10 ml-11 ml-12 ml-14 ml-16 ml-20 ml-24 ml-28 ml-32 ml-36 ml-40
        mt-0 mt-mx mt-0.5 mt-1 mt-1.5 mt-2 mt-2.5 mt-3 mt-3.5 mt-4 mt-5 mt-6 mt-7 mt-8 mt-9 mt-10 mt-11 mt-12 mt-14 mt-16 mt-20 mt-24 mt-28 mt-32 mt-36 mt-40

        -m-0 -m-mx -m-0.5 -m-1 -m-1.5 -m-2 -m-2.5 -m-3 -m-3.5 -m-4 -m-5 -m-6 -m-7 -m-8 -m-9 -m-10 -m-11 -m-12 -m-14 -m-16 -m-20 -m-24 -m-28 -m-32 -m-36 -m-40
        -mr-0 -mr-mx -mr-0.5 -mr-1 -mr-1.5 -mr-2 -mr-2.5 -mr-3 -mr-3.5 -mr-4 -mr-5 -mr-6 -mr-7 -mr-8 -mr-9 -mr-10 -mr-11 -mr-12 -mr-14 -mr-16 -mr-20 -mr-24 -mr-28 -mr-32 -mr-36 -mr-40
        -ml-0 -ml-mx -ml-0.5 -ml-1 -ml-1.5 -ml-2 -ml-2.5 -ml-3 -ml-3.5 -ml-4 -ml-5 -ml-6 -ml-7 -ml-8 -ml-9 -ml-10 -ml-11 -ml-12 -ml-14 -ml-16 -ml-20 -ml-24 -ml-28 -ml-32 -ml-36 -ml-40

        lg:-mt-10 lg:-mt-12 lg:-mt-14 lg:-mt-16 lg:-mt-20 lg:-mt-28

        w-0 w-px w-0.5 w-1 w-1.5 w-2 w-2.5 w-3 w-3.5 w-4 w-5 w-6 w-7 w-8 w-9 w-10 w-11 w-12 w-14 w-16 w-20 w-24 w-28 w-32 w-36
        w-40 w-44 w-48 w-52 w-56 w-60 w-64 w-72 w-80 w-96 w-auto w-1/2 w-1/3 w-2/3 w-1/4 w-2/4 w-3/4 w-1/5 w-2/5 w-3/5 w-4/5 
        w-1/6 w-2/6 w-3/6 w-4/6 w-5/6 w-1/12 w-2/12 w-3/12 w-4/12 w-5/12 w-6/12 w-7/12 w-8/12 w-9/12 w-10/12 w-11/12 
        w-full w-screen w-min w-max

        lg:w-1/3 lg:w-96

        float-left float-right

      `}></div>
    </footer>
  )
}