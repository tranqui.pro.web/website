import { useContext, useEffect } from 'react'
import Link from 'next/link'
import useTranslation from '../lib/translations/useTranslation.hook'
import { LayoutContext } from './layout.context'
import ReactMarkdown from 'react-markdown'
import {
  useViewportScroll
} from "framer-motion"
import AnimatedTrackByScroll from './animatedTrackByScroll'

export default function HeaderMain({ data }) {
  const { t, locale } = useTranslation()
  const { setScrollOnInit } = useContext(LayoutContext)
  // Vertical scroll distance in pixels.
  const { scrollY } = useViewportScroll();

  return (
    <header id="header" className="flex flex-col bg-brand-orange-6 shadow-lg">
      <div className=" bg-gradient-to-br from-brand-azul to-brand-rosa bg-fixed">
        {/* logo */}
        <div className=" lg:hidden">
          <Link href={`/`} scroll={false}>
            <a onClick={() => setScrollOnInit('header')} className="mx-auto inline-block" title={t('common.site-title')}>
              <img src={'/assets/logo-white.svg'} style={{height: '30px'}} className="mt-8 ml-6" />
            </a>
          </Link>
        </div>

        {/* intro */}
        <div className="py-6 mt-24 md:mt-36 md:mb-40 lg:mt-36 lg:mb-32 content-padding">
          {/* logo */}
          <div className="hidden lg:block">
            <Link href={`/`} scroll={false}>
              <a onClick={() => setScrollOnInit('header')} className="mx-auto inline-block" title={t('common.site-title')}>
                <img src={'/assets/logo-white.svg'} style={{height: '80px'}} className="mt-8 ml-6 mb-8" />
              </a>
            </Link>
          </div>

          <h1 className='ml-5 pr-20 md:max-w-xl'>
            <span className='title alt'>{data.text.intro_1}</span>
            <br />
            <span className='sub-title alt'>{data.text.intro_2}</span>
          </h1>

          <a href={`https://wa.me/595991200644?text=${data.text.ctaLinkText[0]}`} target="_blank" rel="noreferrer" >
            <button className='cta mt-12 ml-5 text-brand-lila bg-white hover:bg-indigo-200'>{data.text.saber_mas}</button>
          </a>
        </div>

        {/* mockups images */}
        <div className="mt-32 bg-white/40 py-8 lg:py-12">
          {/* track 1 */}
          <AnimatedTrackByScroll className="lg:pl-20 xl:pl-48 2xl:pl-60">
            <img src="/assets/mockups/mockup-1.jpg" className=' w-[340px] md:w-[500px] lg:w-[570px] lg:mr-10 xl:w-[580px] xl:mr-20 rounded-xl md:rounded-3xl mr-5' />
            <img src="/assets/mockups/mockup-2.jpg" className=' w-[240px] md:w-[280px] lg:w-[300px] lg:mr-10 xl:w-[320px] xl:mr-20 rounded-lg mr-5' />
            <img src="/assets/mockups/mockup-3.jpg" className=' w-[200px] md:w-[300px] lg:w-[320px] lg:mr-10 xl:w-[340px] xl:mr-20 rounded-lg mr-5' />
            <img src="/assets/mockups/mockup-4.jpg" className=' w-[160px] md:w-[230px] lg:w-[260px] lg:mr-10 xl:w-[290px] xl:mr-20 rounded-lg mr-5' />
          </AnimatedTrackByScroll>

          {/* track 2 */}
          <AnimatedTrackByScroll direction="right" className="p-5 lg:py-10 xl:pl-48 2xl:pl-60">
            <img src="/assets/mockups/mockup-5.jpg" className=' w-[310px] md:w-[310px] lg:w-[330px] lg:mr-10 xl:w-[340px] xl:mr-20 rounded-xl mr-5' />
            <img src="/assets/mockups/mockup-6.jpg" className=' w-[230px] md:w-[230px] lg:w-[250px] lg:mr-10 xl:w-[260px] xl:mr-20 rounded-lg mr-5' />
            <img src="/assets/mockups/mockup-7.jpg" className=' w-[270px] md:w-[270px] lg:w-[280px] lg:mr-10 xl:w-[290px] xl:mr-20 rounded-lg mr-5' />
            <img src="/assets/mockups/mockup-8.jpg" className=' w-[150px] md:w-[150px] lg:w-[170px] lg:mr-10 xl:w-[180px] xl:mr-20 rounded-lg mr-5' />
            <img src="/assets/mockups/mockup-9.jpg" className=' w-[240px] md:w-[240px] lg:w-[260px] lg:mr-10 xl:w-[270px] xl:mr-20 rounded-lg mr-5' />
          </AnimatedTrackByScroll>

          {/* track 3 */}
          <AnimatedTrackByScroll className="xl:pl-48 2xl:pl-60">
            <img src="/assets/mockups/mockup-14.jpg" className=' w-[250px] md:w-[250px] lg:w-[250px] lg:mr-10 xl:w-[270px] xl:mr-20 rounded-lg mr-5' />
            <img src="/assets/mockups/mockup-13.jpg" className=' w-[250px] md:w-[250px] lg:w-[250px] lg:mr-10 xl:w-[290px] xl:mr-20 rounded-lg mr-5' />
            <img src="/assets/mockups/mockup-12.jpg" className=' w-[250px] md:w-[250px] lg:w-[250px] lg:mr-10 xl:w-[280px] xl:mr-20 rounded-lg mr-5' />
            <img src="/assets/mockups/mockup-11.jpg" className=' w-[250px] md:w-[250px] lg:w-[250px] lg:mr-10 xl:w-[290px] xl:mr-20 rounded-lg mr-5' />
            <img src="/assets/mockups/mockup-10.jpg" className=' w-[250px] md:w-[250px] lg:w-[250px] lg:mr-10 xl:w-[320px] xl:mr-20 rounded-lg mr-5' />
          </AnimatedTrackByScroll>
        </div>
      </div>
    </header>
  )
}