import { useContext } from 'react'
import Link from 'next/link'
import { Link as LinkScroll } from 'react-scroll'
import ActiveLink from './activeLink'
import useTranslation from '../lib/translations/useTranslation.hook'
import { LayoutContext } from './layout.context'
import LocaleSwitcher from '../components/localeSwitcher'

export default function NavbarHome({ home, fullMenuHeader, localedPathMap }) {
  const { isNavbarOpen, isScrollUnderOffset, setIsNavbarOpen, setScrollOnInit } = useContext(LayoutContext)
  const { t, locale } = useTranslation()
  const showFullHeader = () => fullMenuHeader || isNavbarOpen || isScrollUnderOffset

  const options = {
    // vars
    logo: '/assets/logo-white.svg',
    // classes
    background: 'bg-gradient-to-br from-brand-azul to-brand-lila',
    toggleBtn: 'text-white',
    toggleBtnHover: 'text-white',
    sideMenuMovile: 'bg-gradient-to-br bg-brand-azul opacity-90',
    link: '-mx-4 lg:-mx-3 xl:-mx-2 px-5 py-1 font-normal text-white hover:text-white',
    sublink: 'px-5 pl-10 lg:px-4 py-1 font-normal text-black',
    localeSwitcher: 'py-3 lg:py-4 pl-2 -mx-2 text-white uppercase',
    localeSwitcherOption: ' bg-brand-azul'
  }

  const menuItems = (
    <div className="px-4 lg:px-0 lg:-mx-6 w-full font-semibold text-sm">
      <div className="lg:float-left lg:w-4/5 lg:p-3 lg:ml-6">
        {home ? (<>
          {/* logo link */}
          {showFullHeader() ? (
            <div className="hidden lg:inline cursor-pointer">
              <LinkScroll to="header" smooth={true}>
                <a onClick={() => setScrollOnInit('header')} className="inline-block" title={t('common.site-title')}>
                  <img src={options.logo} style={{height: '20px'}} className="mr-2 relative top-1" />
                </a>
              </LinkScroll>
            </div>
          ) : ''}
          <LinkScroll to="header" smooth={true} spy={true} activeClass="active" offset={0} duration={800} href="" className={`menu-btn ${options.link} font-regular`}>
            {t('common.menu.inicio')}
          </LinkScroll>
        </>) : (<>
          {/* logo link */}
          {showFullHeader() ? (
            <div className="hidden lg:inline cursor-pointer">
              <ActiveLink href={`/`} scroll={false}>
                <a onClick={() => setScrollOnInit('header')} className="inline-block" title={t('common.site-title')}>
                  <img src={options.logo} style={{height: '20px'}} className="mr-2 relative top-1" />
                </a>
              </ActiveLink>
            </div>
          ) : ''}
          <ActiveLink href={`/`} scroll={false} spy={true} activeClass="active" key="homebtn-out">
            <a onClick={() => setScrollOnInit('header')} className={`menu-btn ${options.link}`}>
              {t('common.menu.inicio')}
            </a>
          </ActiveLink>
        </>)}

        <LinkScroll to="beneficios" smooth={true} spy={true} activeClass="active" offset={0} duration={800} href="" className={`menu-btn ${options.link} font-regular`}>
          {t('common.menu.beneficios')}
        </LinkScroll>
        <LinkScroll to="portafolios" smooth={true} spy={true} activeClass="active" offset={0} duration={800} href="" className={`menu-btn ${options.link} font-regular`}>
          {t('common.menu.portafolios')}
        </LinkScroll>
        <LinkScroll to="tecnologias" smooth={true} spy={true} activeClass="active" offset={0} duration={800} href="" className={`menu-btn ${options.link} font-regular`}>
          {t('common.menu.tecnologias')}
        </LinkScroll>
        <LinkScroll to="presupuesto" smooth={true} spy={true} activeClass="active" offset={0} duration={800} href="" className={`menu-btn ${options.link} font-regular`}>
          {t('common.menu.presupuesto')}
        </LinkScroll>
      </div>
      
      <div className="lg:float-right">
        <LocaleSwitcher
          selectClassName={options.localeSwitcher} 
          optionClassName={options.localeSwitcherOption || options.link}
          localedPathMap={localedPathMap}
        ></LocaleSwitcher>
      </div>

      <div className="clear-both"></div>
    </div>
  )
  
  return (
    <>
      <header id="header-menu" className={`header-menu z-5000 h-24 lg:h-12 py-1 lg:py-0 w-full fixed top-0 left-0 ${showFullHeader() ? `shadow-lg ${options.background}` : ''}`}>
        <div className="w-full py-2 lg:hidden">
          {/* navbar header */}
          <div className={`float-left lg:hidden ${showFullHeader() ? 'transition-opacity-full' : 'transition-opacity-none'}`}>
            {/* logo link */}
            <div className="">
              <Link href={`/`} scroll={false}>
                <a onClick={() => setScrollOnInit('header')} className="mx-auto inline-block" title={t('common.site-title')}>
                  <img src={options.logo} style={{height: '30px'}} className="mt-5 ml-6" />
                </a>
              </Link>
            </div>
          </div>
          
          {/* navbar movile trigger */}
          <div className="lg:hidden w-1/5 float-right text-right">
            <button onClick={() => setIsNavbarOpen(!isNavbarOpen)} className="inline-block p-2 pt-5 w-12 text-center text-gray-500 focus:text-gray-700 focus:outline-none" type="button">
              <div className="w-full mx-auto h-auto sm:w-4">
                {isNavbarOpen && (
                  <svg className={`fill-current ${options.toggleBtn} w-auto h-6 mx-auto`} viewBox="0 0 36.44 130.11">
                    <g id="Capa_2" data-name="Capa 2"><g id="Capa_1-2" data-name="Capa 1"><circle className="cls-1" cx="18.22" cy="18.22" r="18.22"/><circle className="cls-1" cx="18.22" cy="65.06" r="18.22"/><circle className="cls-1" cx="18.22" cy="111.89" r="18.22"/></g></g>
                  </svg>
                )}
                {!isNavbarOpen && (
                  <svg className={`fill-current ${options.toggleBtnHover} w-auto h-6 mx-auto`} viewBox="0 0 36.44 130.11">
                    <g id="Capa_2" data-name="Capa 2"><g id="Capa_1-2" data-name="Capa 1"><circle className="cls-1" cx="18.22" cy="18.22" r="18.22"/><circle className="cls-1" cx="18.22" cy="65.06" r="18.22"/><circle className="cls-1" cx="18.22" cy="111.89" r="18.22"/></g></g>
                  </svg>
                )}
              </div>
            </button>
          </div>

          <div className="clear-both"></div>
        </div>

        {/* navbar content desktop */}
        <div className={`hidden lg:flex -mt-1 relative w-full content-padding mx-auto rounded-full ${(!isScrollUnderOffset && !isNavbarOpen) ? '' : ''}`}>
          {menuItems}
        </div>
      </header>

      {/* navbar content mobile */}
      <div className={`z-4500 fixed top-0 left-0 w-1/3 sm:w-1/4 md:w-1/5 h-screen ${options.sideMenuMovile} overflow-y-auto pt-28 pb-4 sm:pt-28 lg:hidden ${isNavbarOpen ? 'block visible' : 'hidden invisible'}`}>        <div className="mb-6">
          {menuItems}
        </div>
      </div>
    </>
  )
}