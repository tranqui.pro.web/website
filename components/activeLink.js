import { useRouter } from 'next/router'
import Link from 'next/link'
import React, { Children } from 'react'

// Agrega la clase activeClass al elemento activo
const ActiveLink = ({ children, activeClass, ...props }) => {
  const { asPath } = useRouter()
  const child = Children.only(children)
  const childClassName = child.props.className || ''

  const className =
    asPath === props.href || asPath === props.as
      ? `${childClassName} ${activeClass}`.trim()
      : childClassName

  return (
    <Link {...props}>
      {React.cloneElement(child, {
        className: className || null,
      })}
    </Link>
  )
}

export default ActiveLink