import AnimatedTrackByScroll from './animatedTrackByScroll'
import SectionFooterEnd from './sectionFooterEnd'
import SectionFooter from './sectionFooter'

export default function Presupuesto({ data }) {
  return (
    <div id="presupuesto">
      {/* mockups images */}
      <div className="mt-24 bg-gradient-to-br from-brand-amarillo to-brand-naranja bg-fixed">
        {/* track */}
        <AnimatedTrackByScroll className="py-10 p-5 lg:pl-32 lg:py-12 xl:pl-48 2xl:pl-72 2xl:py-16" direction="right" getTopOffset={() => (50)}>
          <img src="/assets/mockups/mockup-22.jpg" className=' w-[200px] md:w-[230px] lg:w-[270px] lg:mr-10 xl:w-[300px] xl:mr-16 2xl:w-[440px] 2xl:mr-20 rounded-lg mr-5' />
          <img src="/assets/mockups/mockup-21.jpg" className=' w-[200px] md:w-[230px] lg:w-[270px] lg:mr-10 xl:w-[300px] xl:mr-16 2xl:w-[340px] 2xl:mr-20 rounded-xl mr-5' />
          <img src="/assets/mockups/mockup-20.jpg" className=' w-[200px] md:w-[230px] lg:w-[270px] lg:mr-10 xl:w-[300px] xl:mr-16 2xl:w-[360px] 2xl:mr-20 rounded-lg mr-5' />
          <img src="/assets/mockups/mockup-19.jpg" className=' w-[200px] md:w-[230px] lg:w-[270px] lg:mr-10 xl:w-[300px] xl:mr-16 2xl:w-[300px] 2xl:mr-20 rounded-lg mr-5' />
        </AnimatedTrackByScroll>
      </div>

      <div className=" mt-0 bg-gradient-to-br from-brand-azul to-brand-rosa bg-fixed">  
        {/* presupuesto */}
        <div className="grid grid-flow-row md:grid-flow-col">
          <div className="py-20 content-padding pr-6 lg:max-w-5xl">
            <h2 className='ml-5 titles-padding'>
              <span className='title alt'>{data.text.presupuesto_1}</span>
              <br />
              <span className='sub-title alt'>{data.text.presupuesto_2}</span>
            </h2>

            <div className=" mt-16 text-container alt leading-snug">
              {data.text.presupuesto_content_1}
              <span className="mt-5 block">
                {data.text.presupuesto_content_2}
              </span>
            </div>

            <a href={`https://wa.me/595991200644?text=${data.text.ctaLinkText[3]}`} target="_blank" rel="noreferrer" >
              <button className='cta ml-5 mt-10 bg-white text-brand-lila hover:bg-purple-100'>{data.text.solicitar}</button>
            </a>
          </div>

          <SectionFooter />
        </div>

        <div className="">
          <SectionFooterEnd />
        </div>
      </div>
    </div>
  )
}