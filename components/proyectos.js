import Accordion from './acordion'

  
export default function Proyectos({ data }) {


  const accordionTitles = []
  const accordionItems = data.text.proyectos_list.map(({ title, intro, desc }, index) => {
    accordionTitles.push(title)
    return (
      <div className="" key={index}>
        <div className="text-container">
          {intro}
        </div>
        <div className="text-container bg-white">
          {desc}
        </div>
      </div>
    )
  })


  return (
    <div className="flex flex-col" id="portafolios">
      <div className=" bg-white">
        {/* proyectos */}
        <div className="py-6 mt-24 content-padding">
          <h2 className='ml-5'>
            <span className='title'>{data.text.proyectos_1}</span>
            <span className='sub-title block titles-padding'>{data.text.proyectos_2}</span>
          </h2>
        </div>

        {/* portafolios */}
        <div className="py-5 content-padding">
          <Accordion titles={accordionTitles} items={accordionItems} />
        </div>

        {/* cta */}
        <div className="mt-24 bg-gradient-to-br from-brand-turquesa to-brand-celeste-2 bg-fixed lg:mx-20 lg:rounded-2xl">
          <div className="w-2/3 py-16 m-auto md:scale-110 md:max-w-sm md:py-20 lg:py-28 lg:scale-125 2xl:scale-150">
            <h2 className=' text-xl lg:text2xl text-white text-center font-medium leading-6'>{data.text.escribinos_asesoria}</h2>
            <a href={`https://wa.me/595991200644?text=${data.text.ctaLinkText[2]}`} target="_blank" rel="noreferrer" >
              <button className='cta block m-auto mt-5 bg-white text-brand-celeste hover:bg-lightBlue-100'>{data.text.solicitar}</button>
            </a>
          </div>
        </div>
      </div>

    </div>
  )
}