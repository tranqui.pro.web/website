import { useState } from "react"
import { motion, AnimatePresence } from "framer-motion"
import { colors } from "../config"


const AccordionItem = ({ i, expanded, setExpanded, title, content }) => {
  const isOpen = i === expanded

  return (
    <>
      <motion.div
        className="accordion-header px-5 py-4 font-medium text-2xl lg:text-3xl 2xl:text-4xl rounded-xl"
        initial={false}
        animate={{ 
          color: isOpen ? '#000000' : colors['brand-gray-light']
        }}
        onClick={() => setExpanded(isOpen ? false : i)}
      >
        {title}
      </motion.div>
      <AnimatePresence initial={false}>
        {isOpen && (
          <motion.div
            className="accordion-body"
            key="content"
            initial="collapsed"
            animate="open"
            exit="collapsed"
            variants={{
              open: { opacity: 1, height: "auto" },
              collapsed: { opacity: 0, height: 0 }
            }}
            transition={{ duration: 0.8, ease: [0.04, 0.62, 0.23, 0.98] }}
          >
            <motion.div
              variants={{ collapsed: { scale: 0.8 }, open: { scale: 1 } }}
              transition={{ duration: 0.8 }}
              className="accordion-content"
            >
              {content}
            </motion.div>
          </motion.div>
        )}
      </AnimatePresence>
    </>
  )
}

export default function Accordion({ titles, items }) {
  const [expanded, setExpanded] = useState(0)

  return items.map((content, index) => (
    <AccordionItem i={index} expanded={expanded} setExpanded={setExpanded} title={titles[index]} content={content} key={index} />
  ))
}