import React from 'react'
import ReactMarkdown from 'react-markdown'

export default function SectionPostHeader({imageUrl, mainImageClass, bgColor, title, titleColor, pageDescription}) {
  return (
    <section id="header" className="relative">
      <div className={`relative flex flex-col items-center pt-20 lg:pt-24 h-auto ${bgColor ? 'bg-'+bgColor : 'bg-blue-200'}`}>
        <div className={`${mainImageClass || 'mt-6 w-40 lg:w-64'}`}>
          <img src={imageUrl} className="w-full h-auto" />
        </div>
        <h1 className={`text-3xl md:text-4xl text-center mt-4 mb-10 px-10 uppercase ${titleColor || 'text-brand-purple-1'}`}>{title}</h1>
        <div className={`text-center prose prose-header ${pageDescription ? 'px-14 md:px-36 lg:px-48 xl:px-64 2xl:px-72' : '' } `}>
          <ReactMarkdown>{pageDescription}</ReactMarkdown>
        </div>
      </div>
    </section>
  )
}