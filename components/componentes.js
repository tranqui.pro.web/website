import Accordion from './acordion'

  
export default function Componentes({ data }) {
  const accordionTitles = []
  const accordionItems = data.text.componentes_list.map(({ title, intro, desc }, index) => {
    accordionTitles.push(title)
    return (
      <div className="" key={index}>
        <div className="text-container">
          {desc}
        </div>
      </div>
    )
  })


  return (
    <div className=" bg-white content-padding">
      {/* title */}
      <div className="py-6 mt-24">
        <h2 className='ml-5 titles-padding lg:max-w-2xl'>
          <span className='title'>{data.text.componentes_1}</span>
          <br />
          <span className='sub-title'>{data.text.componentes_2}</span>
        </h2>
      </div>

      {/* componentes */}
      <div className="py-5">
        <Accordion titles={accordionTitles} items={accordionItems} />
      </div>
    </div>
  )
}