import { useContext, useEffect, useState } from 'react'
import Head from 'next/head'
import { animateScroll } from 'react-scroll'
import Navbar from './navbar'
import { LayoutContext } from './layout.context'
import BackToTop from './backToTop'
import config from '../config'


export default function Layout({ 
  children, 
  home, 
  title, image, 
  withPadding, withPaddingY, fullMenuHeader,
  localedPathMap,
  postCategoryItems
}) {
  const pageTitle = title ? `${title} | ${config.siteTitle}` : config.siteTitle
  const { contentRef, isNavbarOpen, setIsNavbarOpen } = useContext(LayoutContext)
  
  
  let mainClasses = 'overflow-x-hidden w-sceen'
  if (withPaddingY) {
    mainClasses += ' pt-24 pb-20'
  }

  // refresh on resize
  useEffect(() => {
    if (typeof window === "undefined") { return }
    function handleResize() {
      animateScroll.scrollMore(1)
      setTimeout(() => {
        animateScroll.scrollMore(1)
      }, 250)
      setTimeout(() => {
        animateScroll.scrollMore(1)
      }, 500)
    }
    window.addEventListener('resize', handleResize)
    return () => {
      window.removeEventListener('resize', handleResize)
    }
  })

  return (
    <>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <title>{pageTitle}</title>
        <meta
          name="description"
          content={pageTitle}
          />
        <meta
          property="og:image"
          content={image}
          />
        <meta name="og:title" content={pageTitle} />
        <meta name="twitter:card" content="summary_large_image" />
      </Head>
      <div id="top"></div>
      <Navbar home={home} fullMenuHeader={fullMenuHeader} localedPathMap={localedPathMap} postCategoryItems={postCategoryItems}></Navbar>
      <BackToTop></BackToTop>
      <button className={`fixed z-4000 w-full h-screen bg-black opacity-50 lg:hidden ${isNavbarOpen ? 'transition-opacity-full' : 'transition-opacity-none'}`} onClick={() => {setIsNavbarOpen(false)}} type="button"></button>
      <main className={mainClasses} ref={contentRef}>{children}</main>
    
      <style global jsx>{``}</style>
    </>
  )
}