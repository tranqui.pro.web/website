import useTranslation from '../lib/translations/useTranslation.hook'

export default function SectionFooterEnd() {
  const { t } = useTranslation()

  return (
    <div className="bg-white/90 text-brand-orange-6 w-full px-10 py-10 md:px-10 lg:py-16 lg:text-lg lg:px-24 xl:px-40 xl:text-xl ">
      <div className="flex justify-center">
        {t('common.hecho-con-amor')}{<img src="/assets/footer-logos/heart.svg" alt="amor" className="beating mx-1"/>}{t('common.en-paraguay')}
      </div>
    </div>
  )
}