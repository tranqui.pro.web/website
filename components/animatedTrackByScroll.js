import { useRef, useContext, useEffect, useState } from 'react'
import {
  motion,
  useMotionValue,
  useSpring,
  useTransform
} from "framer-motion"
import useResizeObserver from '@react-hook/resize-observer'
import { LayoutContext } from './layout.context'


export default function AnimatedTrackByScroll({ children, className, direction, getTopOffset, debug }) {
  useContext(LayoutContext) // no se porqué necesita al context

  const target = useRef(null)
  const elementScrollInView = useMotionValue(0);
  const [ maxTop, setMaxTop ] = useState(0);
  const [ maxWidth, setMaxWidth ] = useState(2000);
  const margins = 15

  useResizeObserver(target, () => {
    const _maxWidth = 0
    target.current.children
    for (const index in target.current.children) {
      if (Object.hasOwnProperty.call(target.current.children, index)) {
        _maxWidth += target.current.children[index].offsetWidth
      }
    }
    _maxWidth += target.current.children.length * margins
    setMaxWidth(_maxWidth)
  })

  useEffect(() => {
    const size = target.current.getBoundingClientRect()
    if (!maxTop) {
      setMaxTop(window.innerHeight + size.height);
    }
    const _topOffset = getTopOffset ? getTopOffset() : 0
    elementScrollInView.set(window.innerHeight + size.height - size.bottom - _topOffset)
    if (debug) {
      console.log(size, size.width)
    }
  })

  const maxWitdhReduced = maxWidth/3
  let positionConfig = [
    [-1, 0, 1, maxTop, maxTop+1], 
    [0, 0, 1, maxWitdhReduced+50, maxWitdhReduced+50]
  ]
  if (direction === 'right') {
    positionConfig = [
      [-1, 0, 1, maxTop, maxTop+1],
      [-maxWitdhReduced, -maxWitdhReduced, -maxWitdhReduced, 0, 0]
    ]
  }

  const positionXRange = useTransform(elementScrollInView, positionConfig[0], positionConfig[1]);
  const positionXValue = useSpring(positionXRange, { stiffness: 300, damping: 50 });

  let styleDef = {
    width: maxWidth
  }

  if (direction === 'right') {
    styleDef.left = positionXValue
  } else {
    styleDef.right = positionXValue
  }

  return (
    <motion.div 
      ref={target} 
      className={`relative flex flex-row place-items-center flex-shrink-0 min-w-[600px] ${className}`}
      style={styleDef}
    >
      {children}
    </motion.div>
  )
} 