import { useEffect, useState, useRef, createContext } from 'react'
import { animateScroll } from 'react-scroll'
import { useRouter } from 'next/router'
import config from '../config'

export const LayoutContext = createContext({
  contentRef: null,
  isNavbarOpen: false,
  setIsNavbarOpen: () => null,
  isScrollUnderOffset: false,
  setScrollUnderOffset: () => null,
  scrollPosition: false,
  setScrollPosition: () => null,
  scrollOnInit: false,
  setScrollOnInit: () => null
})

export const LayoutProvider = ({ children }) => {
  const contentRef = useRef(null)
  const [isNavbarOpen, setIsNavbarOpen] = useState(false)
  const [isScrollUnderOffset, setScrollUnderOffset] = useState(false)
  const [scrollPosition, setScrollPosition] = useState(0)
  const [scrollOnInit, setScrollOnInit] = useState(null)
  const router = useRouter()

  // show full navbar
  const topOffset = config.scrollYtoShowFixedNavbar
  const handleScroll = () => {
    setScrollUnderOffset(window.pageYOffset >= topOffset)
    setScrollPosition(window.pageYOffset)
  }
  useEffect(() => {
    window.addEventListener('scroll', handleScroll)
    window.addEventListener('touchend', handleScroll)
    return () => {
      window.removeEventListener('scroll', () => handleScroll)
      window.removeEventListener('touchend', () => handleScroll)
    }
  }, [])

  // scroll on init
  useEffect(() => {
    const handleRouteChange = () => {
      if (process.browser && scrollOnInit) {
        scrollOnInit && setScrollOnInit(null)
        const elementToScroll = document.getElementById(scrollOnInit)
        if (elementToScroll) {
          animateScroll.scrollTo(elementToScroll.offsetTop, {
            duration: 500,
            delay: 0,
            smooth: true,
            offset: 0
          })
        }
      }
    }
    router.events.on('routeChangeComplete', handleRouteChange)
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [scrollOnInit])

  return <LayoutContext.Provider value={{ 
    contentRef,
    isNavbarOpen,
    setIsNavbarOpen,
    isScrollUnderOffset,
    setScrollUnderOffset,
    scrollPosition,
    setScrollPosition,
    scrollOnInit,
    setScrollOnInit
  }}>{children}</LayoutContext.Provider>
}