import { useContext } from 'react'
import { Link as LinkScroll } from 'react-scroll'
import {
  CircularProgressbarWithChildren,
  buildStyles
} from "react-circular-progressbar"
import {
  motion,
  useSpring,
  useTransform,
  useViewportScroll
} from "framer-motion"
import "react-circular-progressbar/dist/styles.css"
import { LayoutContext } from './layout.context'
import config from '../config'

const buttonMargin = 25;

export default function BackToTop() {
  const { scrollPosition } = useContext(LayoutContext)
  const { scrollY } = useViewportScroll();

  const toTop = {}
  toTop.bottomPositionYRange = useTransform(scrollY, 
    [0, config.scrollYtoShowToTopBtn, config.scrollYtoShowToTopBtn + buttonMargin], 
    [-buttonMargin*3, -buttonMargin*3, buttonMargin]
  );
  toTop.bottomPositionValue = useSpring(toTop.bottomPositionYRange, { stiffness: 400, damping: 40 });

  const getScrollPercentage = () => {
    if (process.browser) {
      return (scrollPosition + window.innerHeight) * 100 / document.documentElement.offsetHeight
    }
  }
  return (
    <LinkScroll to="header" smooth={true} offset={0} duration={config.toTopBtnDuration}>
      <motion.button type="button"
        className={`fixed p-0.5 bg-white rounded-full w-14 h-14 shadow-xl`} 
        style={{
          zIndex: 500,
          right: buttonMargin,
          bottom: toTop.bottomPositionValue
        }}
      >
        <CircularProgressbarWithChildren value={getScrollPercentage()} styles={buildStyles({
          pathColor: config.colors['brand-azul'],
          trailColor: 'white'
        })}>
          <div className="w-6">
            <img src="/assets/logo-pulgar-black.svg" />
          </div>
        </CircularProgressbarWithChildren>
      </motion.button>
    </LinkScroll>
  )
}