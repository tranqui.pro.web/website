export default {
////////////////////////////////
// en
////////////////////////////////
'text_en': {
  'beneficios': 'Beneficios',
  'componentes': 'Componentes',
  'portafolios': 'Portafolios',
  'intro_1': 'Webs y Apps Interactivas e Impresionantes',
  'intro_2': 'para vos y tus clientes',
  'saber_mas': 'Saber más...',
  'abarca_mas_1': 'Abarca más en menos tiempo con Tranqui como aliado',
  'abarca_mas_2': 'en el área de diseño y programación web',
  'abarca_mas_content': 'Ayudamos a las agencias publicitarias a ofrecer webs y apps interactivas e impresionantes a sus clientes dejando en nuestras manos el proceso de creación de sitios webs y landings pages animadas con las mejores tecnologías que optimizan la experiencia del usuario a la vez de disminuir costos.',
  'beneficio_a_1': 'Compromiso',
  'beneficio_a_2': 'Somos un equipo humano experimentado y dinámico',
  'beneficio_a_content': 'Entregamos nuestros resultados en el tiempo establecido, a un precio preferencial y gracias a tecnología de primer nivel somos capaces de realizar cualquier Web y Sistema informático que se necesite.',
  'beneficio_b_1': 'Desarrollo rápido',
  'beneficio_b_2': 'Evite gastos y tiempo perdido con Tranqui como aliado',
  'beneficio_b_content': 'Utilizamos herramientas que simplifican el proceso de diseño y programación optimizando costos y fechas de entrega manteniendo la calidad esperada.',
  'proyectos_1': 'Proyectos relevantes',
  'proyectos_2': 'Realizados por el equipo',
  'proyectos_list': [{
    'title': 'Voceros',
    'intro': 'Una herramienta impulsada por UNICEF que permite a los usuarios publicar denuncias sobre violencia anónimamente.',
    'desc': 'Programación de sitio web y app con mapa interactivo, gráficos estadísticos y panel de gestión interno.'
  }, {
    'title': 'Violencia Digital',
    'intro': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci.',
    'desc': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam.'
  }, {
    'title': 'Covid Al Sur',
    'intro': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci.',
    'desc': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam.'
  }, {
    'title': 'Anti-Pyrawebs',
    'intro': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci.',
    'desc': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam.'
  }, {
    'title': 'Data cuenta',
    'intro': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci.',
    'desc': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam.'
  }, {
    'title': 'Kuña Graffiti',
    'intro': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci.',
    'desc': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam.'
  }],
  'inpulsado': 'Impulsado por',
  'escribinos_asesoria': 'Escribínos para asesorarte en la creación de tus sitios web y apps',
  'contactar': 'Contactar',
  'tecnologias_1': 'Tecnologías',
  'tecnologias_2': 'de última generación',
  'caracteristica_a_1': 'Disminuye costos de mantención Visualiza en un pestañeo',
  'caracteristica_a_2': 'Arquitectura híbrida de contenido estático y bases de datos en la nube',
  'caracteristica_a_content': 'Aprovechá estas tecnologías que disminuyen la necesidad de servidores dedicados a la vez de permitir desplegar en un pestañeo páginas webs y apps con diseños complejos mejorando la experiencia del usuario y el posicionamiento en buscadores.',
  'caracteristica_b_1': 'Llega a más personas',
  'caracteristica_b_2': 'Diseño adaptable a todas las pantallas y Multi-idiomas',
  'caracteristica_b_content': 'Hacé más accesible tu web y tu app ofreciendo tus informaciones en diversos idiomas y mostrando los contenidos de la mejor manera según el dispositivo utilizado por tus usuarios.',
  'tecnologias_list': [{
    'title': 'Next.js',
    'image': 'logos/logo-next.jpg',
    'desc': 'Gestiona el contenido y el funcionamiento de las páginas '
  }, {
    'title': 'Tailwind CSS',
    'image': 'logos/logo-tailwind.jpg',
    'desc': 'Diseña interfaces bellas y complejas sin complicaciones'
  }, {
    'title': 'Realm Mongo DB',
    'image': 'logos/logo-realm.jpg',
    'desc': 'Utiliza bases de datos sin gestionar los servidores.'
  }, {
    'title': 'Ionic',
    'image': 'logos/logo-ionic.jpg',
    'desc': 'Crea apps para todas las plataformas'
  }, {
    'title': 'React',
    'image': 'logos/logo-react.jpg',
    'desc': 'Aporta dinamismo y interacción a las páginas webs.'
  }, {
    'title': 'Node.js',
    'image': 'logos/logo-node.jpg',
    'desc': 'Simplifica la lógica dentro de los servidores en caso de ser necesario.'
  }],
  'componentes_1': 'Proba algunos de los',
  'componentes_2': 'componentes más utilizados',
  'componentes_list': [{
    'title': 'Mapa',
    'desc': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.'
  }, {
    'title': 'Tiras de imágenes',
    'desc': ''
  }, {
    'title': 'Visor de imágenes',
    'desc': ''
  }, {
    'title': 'Cuadrículas de contenido',
    'desc': ''
  }, {
    'title': 'Ventanas flotantes',
    'desc': ''
  }],
  'presupuesto_1': 'Pedí tu presupuesto',
  'presupuesto_2': 'Sin compromiso :)',
  'presupuesto_content_1': 'Los costos de producción en promedio varían entre 2.500.000 gs a 6.500.000 gs según la complejidad.',
  'presupuesto_content_2': '¿Necesitas algo aún más extenso? Tranqui analizaremos el caso y te prepararemos un presupuesto para lo que sea que necesites con formas de pago accesibles.',
  'solicitar': 'Solicitar',
  'ctaLinkText': [
    'Hola Tranqui! Quiero saber más detalles',
    'Hola Tranqui! Hablemos más sobre los beneficios de tenerlos cómo aliados de mi agencia',
    'Hola Tranqui! Quiero solicitar asesoramiento para la creación de mi web/app',
    'Hola Tranqui! Quiero solicitar presupuesto'
  ]
},

////////////////////////////////
// es
////////////////////////////////
'text_es': {
  'beneficios': 'Beneficios',
  'componentes': 'Componentes',
  'portafolios': 'Portafolios',
  'intro_1': 'Webs y Apps Interactivas e Impresionantes',
  'intro_2': 'para vos y tus clientes',
  'saber_mas': 'Saber más...',
  'abarca_mas_1': 'Abarca más en menos tiempo con Tranqui como aliado',
  'abarca_mas_2': 'en el área de diseño y programación web',
  'abarca_mas_content': 'Ayudamos a las agencias publicitarias a ofrecer webs y apps interactivas e impresionantes a sus clientes dejando en nuestras manos el proceso de creación de sitios webs y landings pages animadas con las mejores tecnologías que optimizan la experiencia del usuario a la vez de disminuir costos.',
  'beneficio_a_1': 'Compromiso',
  'beneficio_a_2': 'Somos un equipo humano experimentado y dinámico',
  'beneficio_a_content': 'Entregamos nuestros resultados en el tiempo establecido, a un precio preferencial y gracias a tecnología de primer nivel somos capaces de realizar cualquier Web y Sistema informático que se necesite.',
  'beneficio_b_1': 'Desarrollo rápido',
  'beneficio_b_2': 'Evite gastos y tiempo perdido con Tranqui como aliado',
  'beneficio_b_content': 'Utilizamos herramientas que simplifican el proceso de diseño y programación optimizando costos y fechas de entrega manteniendo la calidad esperada.',
  'proyectos_1': 'Proyectos relevantes',
  'proyectos_2': 'Realizados por el equipo',
  'proyectos_list': [{
    'title': 'Voceros',
    'intro': 'Una herramienta impulsada por UNICEF que permite a los usuarios publicar denuncias sobre violencia anónimamente.',
    'desc': 'Programación de sitio web y app con mapa interactivo, gráficos estadísticos y panel de gestión interno.',
    'desc_2': 'App de registro de denuncias con mapa interactivo y datos de ayuda.'
  }, {
    'title': 'Violencia Digital',
    'intro': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci.',
    'desc': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam.'
  }, {
    'title': 'Covid Al Sur',
    'intro': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci.',
    'desc': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam.'
  }, {
    'title': 'Anti-Pyrawebs',
    'intro': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci.',
    'desc': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam.'
  }, {
    'title': 'Data cuenta',
    'intro': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci.',
    'desc': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam.'
  }, {
    'title': 'Kuña Graffiti',
    'intro': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci.',
    'desc': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam.'
  }],
  'inpulsado': 'Impulsado por',
  'escribinos_asesoria': 'Escribínos para asesorarte en la creación de tus sitios web y apps',
  'contactar': 'Contactar',
  'tecnologias_1': 'Tecnologías',
  'tecnologias_2': 'de última generación',
  'caracteristica_a_1': 'Disminuye costos de mantención Visualiza en un pestañeo',
  'caracteristica_a_2': 'Arquitectura híbrida de contenido estático y bases de datos en la nube',
  'caracteristica_a_content': 'Aprovechá estas tecnologías que disminuyen la necesidad de servidores dedicados a la vez de permitir desplegar en un pestañeo páginas webs y apps con diseños complejos mejorando la experiencia del usuario y el posicionamiento en buscadores.',
  'caracteristica_b_1': 'Llega a más personas',
  'caracteristica_b_2': 'Diseño adaptable a todas las pantallas y Multi-idiomas',
  'caracteristica_b_content': 'Hacé más accesible tu web y tu app ofreciendo tus informaciones en diversos idiomas y mostrando los contenidos de la mejor manera según el dispositivo utilizado por tus usuarios.',
  'tecnologias_list': [{
    'title': 'Next.js',
    'image': 'logos/logo-next.jpg',
    'desc': 'Gestiona el contenido y el funcionamiento de las páginas '
  }, {
    'title': 'Tailwind CSS',
    'image': 'logos/logo-tailwind.jpg',
    'desc': 'Diseña interfaces bellas y complejas sin complicaciones'
  }, {
    'title': 'Realm Mongo DB',
    'image': 'logos/logo-realm.jpg',
    'desc': 'Utiliza bases de datos sin gestionar los servidores.'
  }, {
    'title': 'Ionic',
    'image': 'logos/logo-ionic.jpg',
    'desc': 'Crea apps para todas las plataformas'
  }, {
    'title': 'React',
    'image': 'logos/logo-react.jpg',
    'desc': 'Aporta dinamismo y interacción a las páginas webs.'
  }, {
    'title': 'Node.js',
    'image': 'logos/logo-node.jpg',
    'desc': 'Simplifica la lógica dentro de los servidores en caso de ser necesario.'
  }],
  'componentes_1': 'Proba algunos de los',
  'componentes_2': 'componentes más utilizados',
  'componentes_list': [{
    'title': 'Mapa',
    'desc': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.'
  }, {
    'title': 'Tiras de imágenes',
    'desc': ''
  }, {
    'title': 'Visor de imágenes',
    'desc': ''
  }, {
    'title': 'Cuadrículas de contenido',
    'desc': ''
  }, {
    'title': 'Ventanas flotantes',
    'desc': ''
  }],
  'presupuesto_1': 'Pedí tu presupuesto',
  'presupuesto_2': 'Sin compromiso :)',
  'presupuesto_content_1': 'Los costos de producción en promedio varían entre 2.500.000 gs a 6.500.000 gs según la complejidad.',
  'presupuesto_content_2': '¿Necesitas algo aún más extenso? Tranqui analizaremos el caso y te prepararemos un presupuesto para lo que sea que necesites con formas de pago accesibles.',
  'solicitar': 'Solicitar',
  'ctaLinkText': [
    'Hola Tranqui! Quiero saber más detalles',
    'Hola Tranqui! Hablemos más sobre los beneficios de tenerlos cómo aliados de mi agencia',
    'Hola Tranqui! Quiero solicitar asesoramiento para la creación de mi web/app',
    'Hola Tranqui! Quiero solicitar presupuesto'
  ]
}

////////////////////////////////
// common
////////////////////////////////
    
};
  