module.exports = {
  siteTitle: 'Tranqui',
  // languages
  defaultLocale: 'es',
  locales: ['es', 'en'],

  colors: {
    'brand-celeste': '#00BBF9',
    'brand-celeste-2': '#038EF3',
    'brand-azul': '#5A35FD',
    'brand-turquesa': '#00F5D4',
    'brand-amarillo': '#FEE440',
    'brand-naranja': '#FF6700',
    'brand-lila': '#7209B7',
    'brand-rosa': '#FA8CDD',
    'brand-gray-light': '#A5A5A5',
    'brand-gray-medium': '#7F7F7F'
  },

  toTopBtnDuration: 2000,
  scrollYtoShowToTopBtn: 300,
  scrollYtoShowFixedNavbar: 450
}
